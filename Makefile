.PHONY: all venv clean

ALL_TARGETS := venv

PYTHON_BIN?=python3

all: $(ALL_TARGETS)

venv: venv/bin/activate

venv/bin/activate: requirements.txt
	test -d venv || $(PYTHON_BIN) -m venv venv
	. venv/bin/activate; pip install --upgrade pip wheel
	. venv/bin/activate; pip install -r requirements.txt
	touch venv/bin/activate

clean:
	test -d venv && rm -rf venv || exit 0